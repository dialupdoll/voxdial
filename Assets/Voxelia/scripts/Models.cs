﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static VoxelTreeMonoBehaviour;

public class Models
{
	public class BoneState {
		public Quaternion orientation;
		public Vector3 position;
		public string name;
	}

	public class Pose {
		public BoneState[] bones;
		public string name;
		public BoneState _byName (string searchName) {
			foreach (BoneState bone in bones) {
				if (bone.name.Equals (name)) {
					return bone;
				}
			}
			return null; //not found
		}
		public void _addBoneState (BoneState bone) {
			List<BoneState> magicBox = new List<BoneState> (bones);
			magicBox.Add (bone);
			bones = magicBox.ToArray ();
		}
	}

	public class Bone {
		public VoxelTree image;
		public Vector3 anchorRelative;
		public string boneID = "";
		public Bone parent;
		public GameObject inScene = null;
		public string parentBone;
	}
	

	public class AnimationSkeleton {
		public Bone[] bones;
		public Pose[] poses;

		public Bone byName (string name) {
			foreach (Bone b in bones) {
				if (b.boneID.Equals (name)) {
					return b;
				}
			}
			return null;
		}
	}
}
