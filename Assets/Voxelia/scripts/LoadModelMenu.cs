﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static VoxelTreeMonoBehaviour;
using static Models;

public class LoadModelMenu : MonoBehaviour
{
	VoxelTree model = null;
	public GameObject pathInput;
	public GameObject bonePreview = null;
	string lastLoaded = null;
	UnityEngine.UI.InputField text;

	public GameObject baseForRender;

	public GameObject goCamera;

	public Material material;

	bool isValidPath;
	public OmniboxelCentral central;

	private void Start () {
		model = null;
		Destroy (bonePreview);
		text = pathInput.GetComponent<UnityEngine.UI.InputField> ();
	}

	public string CurrentPath () {
		return text.text;
	}

	public void ValidatePath () {
		isValidPath = System.IO.File.Exists (CurrentPath ());
	}

	public void PreviewClicked () {
		ValidatePath ();
		LoadModel (CurrentPath ());
	}

	public void ConfirmClicked () {
		ValidatePath ();
		LoadModel (CurrentPath ());
		Bone bone = new Bone ();
		bone.boneID = "Unnamed";
		bone.image = model;

		central._addBone (bone);
		central._updateRenderGeometry ();
		central._updateRenderPositioning ();
		

		ReturnToCentral ();
	}

	public void ReturnToCentral () {
		central.gameObject.SetActive (true);
		this.gameObject.SetActive (false);
		central.renderObject.SetActive (true);
		central._revalidateArrays ();
		Destroy (bonePreview);
	}

	void LoadModel (string path) {
		model = new VoxelTree.SimpleSparseGrid ();
		importFromGoxelTextFile (path, model);
		Destroy (bonePreview);
		bonePreview = model.render (baseForRender, material);
		goCamera.transform.position = new Vector3 (0, 0, -model.root.asBounds().size.magnitude * 1.1f);
		//bonePreview.transform.position = model.root.asBounds ().size / 2;
		bonePreview.transform.position = -model.root.asBounds ().center / 2;
		if (model != null) {
			lastLoaded = path;
			bonePreview = model.render (baseForRender, material);
			bonePreview.transform.parent = baseForRender.transform;
			Mesh m = bonePreview.GetComponent<MeshFilter> ().mesh;
			m.RecalculateNormals ();
			m.RecalculateTangents ();
			//Debug.Log (m.colors[0].ToString());
		}
	}
}
