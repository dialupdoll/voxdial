﻿
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEditor;

/*
ISpecialType t = obj as ISpecialType;

if (t != null)
{
    // use t here
}
^ USE FOR EFFICIENT TYPECHECKING WHEN YOU ARE ALREADY CASTING TO THAT TYPE AFTER
 */
public class VoxelTreeMonoBehaviour : MonoBehaviour {
	// DECISIONS TO BE MADE:
	// 
	public abstract class VoxelTree {
		public VoxelTreeNode root = null;
		public abstract void Add (VoxelTree tree, VoxelTree.VoxelTreeNode node);

		public static bool quadrantCollisionCheck (Bounds node, Vector3 one, Vector3 two) {
			return quadrantID (node, one) == quadrantID (node, two);

		}

		public virtual VoxelTreeNode getNode (Vector3 position) {
			return null;
		}

		public static sbyte quadrantID (Bounds nodeBounds, Vector3 sub) {
			Vector3 node = nodeBounds.center;
			if (sub.z == node.z | sub.y == node.y | sub.x == node.x) {
				return -1;
			}
			sbyte result = 0;
			if (sub.x < node.x)
				result++;
			if (sub.y < node.y)
				result += 2;
			if (sub.z > node.z)
				result += 4;
			return result;

		}

		public abstract void drawDebug ();
		public abstract GameObject render (GameObject parent, Material defaultMaterial);

		public abstract class VoxelTreeNode {
			public abstract VoxelTreeNode[] children ();

			public abstract Bounds asBounds ();

			public abstract VoxelTreeNode containingChildOf (Vector3 position);
			//should return null if not found
			public abstract int nodeSize ();
			// node size is a measure of how many "one unit" voxels or nodes are contained within this node and it's children, across any one face. like the diameter of a square

			public abstract void drawDebug ();


			public abstract bool addNode (VoxelTreeNode node);

			public virtual bool setNode (VoxelTreeNode node) {
				return false;
			}

			public virtual Renderable asRenderable () {
				return null;
			}

			public VoxelTreeNode getChildByQuadrant (sbyte quadrant) {
				return null;
			}

			public class SimpleDebugContentNode : VoxelTreeNode, Renderable {
				Vector3 position;
				Color color;

				public SimpleDebugContentNode (Vector3 location, Color shade) {
					position = location;
					color = shade;
					//color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
				}

				public override bool addNode (VoxelTreeNode node) {
					return false;
				}

				public override Bounds asBounds () {
					return new Bounds (position, new Vector3 (1, 1, 1));
				}

				public override VoxelTreeNode[] children () {
					return null;
				}

				public override VoxelTreeNode containingChildOf (Vector3 position) {
					if (asBounds ().Contains (position)) {
						return this;
					} else {
						return null;
					}
				}

				public override void drawDebug () {
					Gizmos.color = color;
					Gizmos.DrawCube (position, new Vector3 (1, 1, 1));
				}

				public override int nodeSize () {
					return 1; // content nodes are always minimum size
				}

				public MeshSubsection render (VoxelTree tree) {
					//Mesh result = new Mesh();
					Vector3 center = asBounds ().center;
					//List<CombineInstance> combiner = new List<CombineInstance>();
					MeshSubsection resultMS = null;


					// top
					if (tree.getNode (center + new Vector3 (0, 1, 0)) == null) {
						MeshSubsection side = new MeshSubsection (true, false, false);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);

						side.vertices.Add (center + new Vector3 (-0.5f, 0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, 0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, 0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, 0.5f, -0.5f));

						side.triangles.Add (0);
						side.triangles.Add (1);
						side.triangles.Add (2);

						side.triangles.Add (0);
						side.triangles.Add (2);
						side.triangles.Add (3);

						//Mesh combinatorMesh = side.convert();
						//combinatorMesh.UploadMeshData(false);
						//CombineInstance ci = new CombineInstance();
						//ci.mesh = combinatorMesh;
						//combiner.Add(ci);
						if (resultMS == null) {
							resultMS = side;
						} else {
							resultMS = MeshSubsection.Stitch (resultMS, side);
						}
					}

					// bottom
					if (tree.getNode (center + new Vector3 (0, -1, 0)) == null) {
						MeshSubsection side = new MeshSubsection (true, false, false);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);

						side.vertices.Add (center + new Vector3 (-0.5f, -0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, -0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, -0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, -0.5f, -0.5f));

						side.triangles.Add (3);
						side.triangles.Add (2);
						side.triangles.Add (1);

						side.triangles.Add (3);
						side.triangles.Add (1);
						side.triangles.Add (0);

						//Mesh combinatorMesh = side.convert();
						//combinatorMesh.UploadMeshData(false);
						//CombineInstance ci = new CombineInstance();
						//ci.mesh = combinatorMesh;
						if (resultMS == null) {
							resultMS = side;
						} else {
							resultMS = MeshSubsection.Stitch (resultMS, side);
						}
					}

					// +X
					if (tree.getNode (center + new Vector3 (1, 0, 0)) == null) {
						MeshSubsection side = new MeshSubsection (true, false, false);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);

						side.vertices.Add (center + new Vector3 (0.5f, -0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, -0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, 0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, 0.5f, -0.5f));

						side.triangles.Add (3);
						side.triangles.Add (2);
						side.triangles.Add (1);

						side.triangles.Add (3);
						side.triangles.Add (1);
						side.triangles.Add (0);

						//Mesh combinatorMesh = side.convert();
						//combinatorMesh.UploadMeshData(false);
						//CombineInstance ci = new CombineInstance();
						//ci.mesh = combinatorMesh;
						if (resultMS == null) {
							resultMS = side;
						} else {
							resultMS = MeshSubsection.Stitch (resultMS, side);
						}
					}

					// -X
					if (tree.getNode (center + new Vector3 (-1, 0, 0)) == null) {
						MeshSubsection side = new MeshSubsection (true, false, false);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);

						side.vertices.Add (center + new Vector3 (-0.5f, -0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, -0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, 0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, 0.5f, -0.5f));

						side.triangles.Add (1);
						side.triangles.Add (2);
						side.triangles.Add (3);

						side.triangles.Add (0);
						side.triangles.Add (1);
						side.triangles.Add (3);

						//Mesh combinatorMesh = side.convert();
						//combinatorMesh.UploadMeshData(false);
						//CombineInstance ci = new CombineInstance();
						//ci.mesh = combinatorMesh;
						if (resultMS == null) {
							resultMS = side;
						} else {
							resultMS = MeshSubsection.Stitch (resultMS, side);
						}
					}

					// +z
					if (tree.getNode (center + new Vector3 (0, 0, 1)) == null) {
						MeshSubsection side = new MeshSubsection (true, false, false);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);

						side.vertices.Add (center + new Vector3 (-0.5f, -0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, 0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, 0.5f, 0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, -0.5f, 0.5f));

						side.triangles.Add (3);
						side.triangles.Add (2);
						side.triangles.Add (1);

						side.triangles.Add (3);
						side.triangles.Add (1);
						side.triangles.Add (0);

						//Mesh combinatorMesh = side.convert();
						//combinatorMesh.UploadMeshData(false);
						//CombineInstance ci = new CombineInstance();
						//ci.mesh = combinatorMesh;
						if (resultMS == null) {
							resultMS = side;
						} else {
							resultMS = MeshSubsection.Stitch (resultMS, side);
						}
					}

					// -z
					if (tree.getNode (center + new Vector3 (0, 0, -1)) == null) {
						MeshSubsection side = new MeshSubsection (true, false, false);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);
						side.colors.Add (color);

						side.vertices.Add (center + new Vector3 (-0.5f, -0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (-0.5f, 0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, 0.5f, -0.5f));
						side.vertices.Add (center + new Vector3 (0.5f, -0.5f, -0.5f));

						side.triangles.Add (1);
						side.triangles.Add (2);
						side.triangles.Add (3);

						side.triangles.Add (0);
						side.triangles.Add (1);
						side.triangles.Add (3);

						//Mesh combinatorMesh = side.convert();
						//combinatorMesh.UploadMeshData(false);
						//CombineInstance ci = new CombineInstance();
						//ci.mesh = combinatorMesh;
						if (resultMS == null) {
							resultMS = side;
						} else {
							resultMS = MeshSubsection.Stitch (resultMS, side);
						}
					}

					return resultMS;
				}


			}

			public class SimpleSparseBoundsNode : VoxelTreeNode {
				Bounds space;
				public VoxelTreeNode[] m_children = new VoxelTreeNode[8];

				public SimpleSparseBoundsNode (Vector3 position, Vector3 size) {
					space = new Bounds (position, size);
				}

				public override Bounds asBounds () {
					return new Bounds (space.center, space.size);
				}

				public override VoxelTreeNode[] children () {
					return m_children;
				}

				public new VoxelTreeNode getChildByQuadrant (sbyte quadrant) {
					return m_children[quadrant];
				}

				public override VoxelTreeNode containingChildOf (Vector3 position) {
					throw new System.NotImplementedException ();
				}

				public override int nodeSize () {
					throw new System.NotImplementedException ();
				}

				public override bool addNode (VoxelTreeNode node) {
					if (!asBounds ().Contains (node.asBounds ().center)) {
						Debug.Log ("too small!");
						return false;
					}
					sbyte qID = VoxelTree.quadrantID (asBounds (), node.asBounds ().center);
					if (m_children[qID] != null) {
						Debug.Log ("slot taken!");
						return false;
					}
					m_children[qID] = node;
					return true;
				}

				public new bool setNode (VoxelTreeNode node) {
					if (!asBounds ().Contains (node.asBounds ().center)) {
						Debug.Log ("too small!");
						return false;
					}
					sbyte qID = VoxelTree.quadrantID (asBounds (), node.asBounds ().center);
					m_children[qID] = node;
					return true;
				}

				public override void drawDebug () {
					Gizmos.color = Color.red;
					Gizmos.DrawWireCube (asBounds ().center, asBounds ().size);
				}
			}

			public class SimpleSparseGridNode : VoxelTreeNode {
				Vector3 location;
				int diameter = 0;
				VoxelTreeNode[] m_children = new VoxelTreeNode[8];

				public SimpleSparseGridNode (Vector3 location, int diameter) {
					this.location = location;
					this.diameter = diameter;
				}

				public override Bounds asBounds () {
					Bounds result = new Bounds (location, new Vector3 (diameter, diameter, diameter));
					return result;
				}

				public override VoxelTreeNode[] children () {
					return m_children;
				}

				public override VoxelTreeNode containingChildOf (Vector3 position) {
					if (asBounds ().Contains (position)) {
						foreach (VoxelTreeNode node in m_children) {
							if (node != null) {
								if (node.asBounds ().Contains (position)) {
									return node;
								}
							}
						}
					}

					return null;
				}

				public override int nodeSize () {
					return diameter;
				}

				public override void drawDebug () {
					int x = 1;
					bool y = false;
					while (x < nodeSize ()) {
						x = x * 2;
						y = !y;
					}
					if (y) {

						Gizmos.color = Color.blue;
					} else {

						Gizmos.color = Color.red;
					}
					Gizmos.color = new Color (Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.5f);
					Gizmos.DrawWireCube (asBounds ().center, asBounds ().size);
				}

				public override bool addNode (VoxelTreeNode node) {
					//if (node.nodeSize() == diameter / 2) {

					/*sbyte qID = quadrantID(asBounds(), node.asBounds().center);
					if (qID == -1)
					{
					    Debug.Log("perfectly centered, for some reason. not on the grid.");
					    return false;
					}
					else
					{
					    if (m_children[qID] == null)
					    {
						   m_children[qID] = node;
						   return true;
					    }
					    else
					    {
						   Debug.Log("already a voxel there! should be set node.");
						   return false;
					    }
					}*/
					for (int x = 0; x < m_children.Length; x++) {
						if (m_children[x] == null) {
							m_children[x] = node;
							return true;
						}
					}
					/*} else
					{
					    Debug.Log("shouldn't add there! wrong size");
					    return false;
					}*/
					return false;
				}
			}
		}

		public class SimpleSparseGrid : VoxelTree {
			public Vector3 createChild (VoxelTree.VoxelTreeNode parent, Vector3 direction) {
				Vector3 node = parent.asBounds ().center;
				int size = parent.nodeSize ();

				if (direction.z == node.z | direction.y == node.y | direction.x == node.x) {
					//throw new System.Exception("node is positioned exactly along the same axis as the grid, in a way that does not permit addition of child");
				}

				Vector3 result = Vector3.zero;

				if (direction.z > node.z) {
					result.z = node.z + (size / 4);
				} else {
					result.z = node.z - (size / 4);
				}

				if (direction.y > node.y) {
					result.y = node.y + (size / 4);
				} else {
					result.y = node.y - (size / 4);
				}

				if (direction.x > node.x) {
					result.x = node.x + (size / 4);
				} else {
					result.x = node.x - (size / 4);
				}

				return result;
			}
			public Vector3 createParent (VoxelTree.VoxelTreeNode child, Vector3 direction) {

				Vector3 node = child.asBounds ().center;
				int size = child.nodeSize ();

				if (direction.z == node.z | direction.y == node.y | direction.x == node.x) {
					//throw new System.Exception("node is positioned exactly along the same axis as the grid, in a way that does not permit expansion");
				}

				Vector3 result = Vector3.zero;

				if (direction.z > node.z) {
					result.z = node.z + (size / 2);
				} else {
					result.z = node.z - (size / 2);
				}

				if (direction.y > node.y) {
					result.y = node.y + (size / 2);
				} else {
					result.y = node.y - (size / 2);
				}

				if (direction.x > node.x) {
					result.x = node.x + (size / 2);
				} else {
					result.x = node.x - (size / 2);
				}

				return result;
			}
			public override void Add (VoxelTree tree, VoxelTree.VoxelTreeNode node) {
				try {

					if (tree.root == null) {

						VoxelTree.VoxelTreeNode nRoot = new VoxelTree.VoxelTreeNode.SimpleSparseGridNode (node.asBounds ().center + new Vector3 (0.5f, 0.5f, 0.5f), 2);
						nRoot.addNode (node);
						tree.root = nRoot;
						return;
					}
					if (tree.root.asBounds ().Contains (node.asBounds ().center)) {

						VoxelTree.VoxelTreeNode current = tree.root;
						while (true) {
							if (current.nodeSize () > 2) {
								VoxelTree.VoxelTreeNode child = current.containingChildOf (node.asBounds ().center);
								if (child != null) {
									current = child;
								} else {
									child = new VoxelTree.VoxelTreeNode.SimpleSparseGridNode (createChild (current, node.asBounds ().center), current.nodeSize () / 2);
									current.addNode (child);
									current = child;
								}
							} else {
								bool success = current.addNode (node);
								if (!success) {
									Debug.Log ("failed to add?");
								}
								return;
							}
						}

					} else {
						VoxelTree.VoxelTreeNode current = tree.root;
						while (true) {
							VoxelTree.VoxelTreeNode parent = new VoxelTree.VoxelTreeNode.SimpleSparseGridNode (createParent (current, node.asBounds ().center), current.nodeSize () * 2);
							bool success = parent.addNode (current);
							if (!success) {
								Debug.Log ("failed to expand tree?");
								return;
							}
							if (parent.asBounds ().Contains (node.asBounds ().center)) {
								success = parent.addNode (node);
								if (!success) {
									Debug.Log ("failed to add?");
									return;
								}
								tree.root = parent;
								return;
							} else {
								current = parent;
							}
						}
					}
				} catch (System.Exception e) {
					Debug.Log (e.Message);
				}
			}


			public override VoxelTree.VoxelTreeNode getNode (Vector3 position) {

				if (root != null) {
					List<VoxelTree.VoxelTreeNode> nodesToSearch = new List<VoxelTree.VoxelTreeNode> ();
					if (root.asBounds ().Contains (position)) {
						nodesToSearch.Add (root);
					} else {
						return null;
					}

					while (nodesToSearch.Count > 0) {
						VoxelTree.VoxelTreeNode curNode = nodesToSearch.ToArray ()[0];
						nodesToSearch.RemoveAt (0);

						if (curNode.nodeSize () == 1) {
							if (curNode.asBounds ().Contains (position)) {
								return curNode;
							}
						} else {
							if (curNode.children () != null) {
								foreach (VoxelTree.VoxelTreeNode node in curNode.children ()) {
									if (node != null) {
										if (node.asBounds ().Contains (position)) {
											nodesToSearch.Add (node);
										}
									}
								}
							}
						}
					}
				}
				return null;
			}

			public override void drawDebug () {
			}
			GameObject renderRoot = null;

			public override GameObject render (GameObject parent, Material defaultMaterial) {
				if (renderRoot == null) {
					renderRoot = new GameObject ("render");
					renderRoot.transform.parent = parent.transform;
					renderRoot.AddComponent<MeshFilter> ();
					renderRoot.AddComponent<MeshRenderer> ();
				}
				//mesh = new Mesh();
				//mesh.name = parent.name;
				MeshSubsection combined = null;


				if (root != null) {
					List<VoxelTree.VoxelTreeNode> nodesToDraw = new List<VoxelTree.VoxelTreeNode> ();
					nodesToDraw.Add (root);

					while (nodesToDraw.Count > 0) {
						VoxelTree.VoxelTreeNode curNode = nodesToDraw.ToArray ()[0];
						nodesToDraw.RemoveAt (0);
						if (curNode.nodeSize () == 1) {
							Renderable r = curNode as Renderable;
							if (r != null) {
								MeshSubsection result = r.render (this);


								if (combined == null) {
									combined = result;
								} else {
									combined = MeshSubsection.Stitch (combined, result);
								}

								//CombineInstance ci = new CombineInstance();
								//ci.mesh = result;
								//combiner.Add(ci);
							}



						} else if (curNode.children () != null) {
							foreach (VoxelTree.VoxelTreeNode node in curNode.children ()) {
								if (node != null)
									nodesToDraw.Add (node);
							}
						}
					}

					//mesh.CombineMeshes(combiner.ToArray(), true);
					MeshFilter mf = renderRoot.GetComponent<MeshFilter> ();
					mf.mesh = combined.convert ();
					mf.mesh.RecalculateNormals ();
					mf.mesh.RecalculateTangents ();
					MeshRenderer mr = renderRoot.GetComponent<MeshRenderer> ();
					mr.material = defaultMaterial;
				}
				return renderRoot;
			}

		}

	}
	public Material defaultMaterial = null;
	public class MeshSubsection {
		public List<int> triangles = null;
		public List<Vector3> vertices = null;
		public List<Color> colors = null;
		public List<Vector4> tangents = null;
		public List<Vector2> uv = null;

		public MeshSubsection (bool col, bool tan, bool tex) {
			init (col, tan, tex);
		}

		public MeshSubsection () {
			init (false, false, false);
		}

		private void init (bool col, bool tan, bool tex) {
			triangles = new List<int> ();
			vertices = new List<Vector3> ();
			if (col) {
				colors = new List<Color> ();
			}
			if (tan) {
				tangents = new List<Vector4> ();
			}
			if (tex) {
				uv = new List<Vector2> ();
			}
		}

		public static MeshSubsection Stitch (MeshSubsection ms1, MeshSubsection ms2) {
			if (ms1 == null) {
				return ms2;
			}
			if (ms2 == null) {
				return ms1;
			}
			bool colors = false;
			if (ms1.colors != null & ms2.colors != null)
				colors = true;
			MeshSubsection result = new MeshSubsection {
				vertices = new List<Vector3> (ms1.vertices),
				triangles = new List<int> (ms1.triangles),
				colors = new List<Color> (ms1.colors)
			};

			for (int i = 0; i < ms2.triangles.Count; i++) {
				result.triangles.Add (ms2.triangles[i] + ms1.vertices.Count);
			}
			for (int i = 0; i < ms2.vertices.Count; i++) {
				result.vertices.Add (ms2.vertices[i]);
			}
			if (colors) {
				for (int i = 0; i < ms2.colors.Count; i++) {
					result.colors.Add (ms2.colors[i]);
				}
			}
			return result;


		}
		public Mesh convert () {
			Mesh result = new Mesh ();
			result.vertices = vertices.ToArray ();
			result.triangles = triangles.ToArray ();
			result.colors = colors.ToArray ();
			return result;
		}
	}

	public interface Renderable {
		MeshSubsection render (VoxelTree tree);
	}

	public VoxelTree tree = null;

	void Start () {
		/*
		  Random.InitState (System.DateTime.UtcNow.Millisecond);
		  for (int x = 0; x < 100; x++)
		{
		    Vector3 rand = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
		    rand.Normalize();
		    rand = rand * 10;
		    rand.x = Mathf.RoundToInt(rand.x);
		    rand.y = Mathf.RoundToInt(rand.y);
		    rand.z = Mathf.RoundToInt(rand.z);
		    Color c = new Color32(100, (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255));
		    VoxelTree.VoxelTreeNode node = new VoxelTree.VoxelTreeNode.SimpleDebugContentNode (rand, c);
		    tree.Add (tree, node);
		    tree.render(this.gameObject);
		  }*/

		tree = new VoxelTree.SimpleSparseGrid ();
		importFromGoxelTextFile("test.txt", tree);
		tree.render(this.gameObject, defaultMaterial);

		//Mesh m = this.GetComponent<MeshFilter> ().mesh;
		//m.RecalculateNormals ();

	}

	// Update is called once per frame
	void Update () {
	}

	//int drawCount = 0;
	/*void OnDrawGizmos () {
		if (!debugMode)
			return;
		if (shouldRenderContentNodes | shouldRenderWireframes) {
			drawCount = 0;
			List<VoxelTree.VoxelTreeNode> nodesToDraw = new List<VoxelTree.VoxelTreeNode> ();
			if (tree != null) {
				if (tree.root != null) {
					nodesToDraw.Add (tree.root);
					while (nodesToDraw.Count > 0) {
						VoxelTree.VoxelTreeNode curNode = nodesToDraw.ToArray ()[0];
						nodesToDraw.RemoveAt (0);
						if (curNode.nodeSize () == 1 & shouldRenderContentNodes) {
							curNode.drawDebug ();
							drawCount++;
						} else if (curNode.nodeSize () != 1 & shouldRenderWireframes) {
							curNode.drawDebug ();
							drawCount++;
						}
						if (curNode.children () != null) {
							foreach (VoxelTree.VoxelTreeNode node in curNode.children ()) {
								if (node != null)
									nodesToDraw.Add (node);
							}
						}
					}
				}
			}
			Handles.color = Color.magenta;
			Handles.Label (transform.position, "Total nodes drawn: " + drawCount);
		}
	}*/
	[Header("Debug")]
	public bool debugMode = false;
	[ConditionalHide("debugMode")]
	public bool shouldRenderContentNodes = false;
	[ConditionalHide("debugMode")]
	public bool shouldRenderWireframes = false;
	public static VoxelTree importFromGoxelTextFile (string location, VoxelTree target) {
		string[] lines = System.IO.File.ReadAllLines (location);
		foreach (string line in lines) {
			if (line.StartsWith ("#")) {
				continue; // skip this line, its a comment
			} else {
				//Quaternion rotation = Quaternion.EulerRotation(new Vector3(-60, 0, 0));
				string[] data = line.Split (' ');
				Vector3 position;
				if (data.Length < 4) {
					Debug.LogWarning ("File format is weird, continuing anyway...");
					continue; // this isn't a valid line! discard it.
				}
				position.x = float.Parse (data[1]);
				position.y = float.Parse (data[2]);
				position.z = float.Parse (data[0]);
				//position = rotation * position;

				string colorcode = data[3];
				Color result = new Color (int.Parse (colorcode.Substring (0, 2), NumberStyles.HexNumber), int.Parse (colorcode.Substring (2, 2), NumberStyles.HexNumber), int.Parse (colorcode.Substring (4, 2), NumberStyles.HexNumber));
				VoxelTree.VoxelTreeNode node = new VoxelTree.VoxelTreeNode.SimpleDebugContentNode (position, result);
				target.Add (target, node);
			}
		}
		return target;
	}
}
