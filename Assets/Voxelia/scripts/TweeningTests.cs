﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweeningTests : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
        VoxelTreeMonoBehaviour vox = transform.GetComponent<VoxelTreeMonoBehaviour>();
        vox.tree = new VoxelTreeMonoBehaviour.VoxelTree.SimpleSparseGrid();
        for (int x = 0;  x <= 100;  x++)
        {

            float y = Tweening.FloatTween.cubicMirrored(x, 0, 1, 100);
            vox.tree.Add(vox.tree, new VoxelTreeMonoBehaviour.VoxelTree.VoxelTreeNode.SimpleDebugContentNode(new Vector3(x, y * 100, 0), Color.red));
        }
        vox.tree.render(transform.gameObject, vox.defaultMaterial);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
