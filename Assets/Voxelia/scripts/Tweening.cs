﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tweening : MonoBehaviour {
	public abstract class Quaternion {

	}

	public abstract class FloatTween
	/// start and change should be zero
	/// and one for 0-1 times multiplicative scaling.
	{
		public static float linear (float time, float start, float change, float duration) {
			return change * time / duration + start;
		}
		public static float quadraticIn (float time, float start, float change, float duration) {
			time /= duration;
			return change * time * time + start;
		}
		public static float quadraticOut (float time, float start, float change, float duration) {
			time /= duration;
			return -change * time * (time - 2) + start;
		}
		public static float quadraticMirrored (float time, float start, float change, float duration) {
			time /= duration / 2;
			if (time < 1)
				return change / 2 * time * time + start;
			time--;
			return -change / 2 * (time * (time - 2) - 1) + start;
		}
		public static float cubicIn (float time, float start, float change, float duration) {
			time /= duration;
			return change * time * time * time + start;
		}
		public static float cubicOut (float time, float start, float change, float duration) {
			time /= duration;
			time--;
			return change * (time * time * time + 1) + start;
		}
		public static float cubicMirrored (float time, float start, float change, float duration) {
			time /= duration / 2;
			if (time < 1)
				return change / 2 * time * time * time + start;
			time -= 2;
			return change / 2 * (time * time * time + 2) + start;
		}
		public static float quarticIn (float time, float start, float change, float duration) {
			time /= duration;
			return change * time * time * time * time + start;
		}
		public static float quarticOut (float time, float start, float change, float duration) {
			time /= duration;
			time--;
			return -change * (time * time * time * time - 1) + start;
		}
		public static float quarticMirrored (float time, float start, float change, float duration) {
			time /= duration / 2;
			if (time < 1)
				return change / 2 * time * time * time * time + start;
			time -= 2;
			return -change / 2 * (time * time * time * time - 2) + start;
		}
		public static float quinticIn (float time, float start, float change, float duration) {
			time /= duration;
			return change * time * time * time * time * time + start;
		}
		public static float quinticOut (float time, float start, float change, float duration) {
			time /= duration;
			time--;
			return change * (time * time * time * time * time + 1) + start;
		}
		public static float quinticMirrored (float time, float start, float change, float duration) {
			time /= duration / 2;
			if (time < 1)
				return change / 2 * time * time * time * time * time + start;
			time -= 2;
			return change / 2 * (time * time * time * time * time + 2) + start;
		}
		public static float sinusoidalIn (float time, float start, float change, float duration) {
			return change * Mathf.Sin (time / duration * (Mathf.PI / 2)) + start;
		}
		public static float sinusoidalOut (float time, float start, float change, float duration) {
			return change * Mathf.Sin (time / duration * (Mathf.PI / 2)) + start;
		}
		public static float sinusoidalMirrored (float time, float start, float change, float duration) {
			return -change / 2 * (Mathf.Cos (Mathf.PI * time / duration) - 1) + start;
		}
		public static float exponentialIn (float time, float start, float change, float duration) {
			return change * Mathf.Pow (2, 10 * (time / duration - 1)) + start;
		}
		public static float exponentialOut (float time, float start, float change, float duration) {
			return change * (-Mathf.Pow (2, -10 * time / duration) + 1) + start;
		}
		public static float exponentialMirrored (float time, float start, float change, float duration) {
			time /= duration / 2;
			if (time < 1)
				return change / 2 * Mathf.Pow (2, 10 * (time - 1)) + start;
			time--;
			return change / 2 * (-Mathf.Pow (2, -10 * time) + 2) + start;
		}
		public static float circularIn (float time, float start, float change, float duration) {
			time /= duration;
			return -change * (Mathf.Sqrt (1 - time * time) - 1) + start;
		}
		public static float circularOut (float time, float start, float change, float duration) {
			time /= duration;
			time--;
			return change * Mathf.Sqrt (1 - time * time) + start;
		}
		public static float circularMirrored (float time, float start, float change, float duration) {
			time /= duration / 2;
			if (time < 1)
				return -change / 2 * (Mathf.Sqrt (1 - time * time) - 1) + start;
			time -= 2;
			return change / 2 * (Mathf.Sqrt (1 - time * time) + 1) + start;
		}
	}

}
