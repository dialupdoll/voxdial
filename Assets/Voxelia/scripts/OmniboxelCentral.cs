﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OmniboxelCentral : MonoBehaviour {
	public GameObject loadModelMenu;
	public Dropdown boneList;
	Models.Bone selected = null;
	public InputField runtimeBoneName;

	public GameObject cameraGO;
	public GameObject renderParent;
	public GameObject renderObject = null;

	public Models.AnimationSkeleton skeleton = null;
	public Models.Pose tPose;
	Models.Pose currentPose;

	public Material renderMaterial;

	#region bone selection
	public void AddBonePressed () {
		renderObject.SetActive (false);
		loadModelMenu.SetActive (true);
		gameObject.SetActive (false);
	}

	public void BoneRenamed () {
		if (skeleton.bones.Length == 0)
			return;

		selected = skeleton.bones[boneList.value];

		selected.boneID = runtimeBoneName.text;
		refreshOptions ();
	}

	public void BoneSelected () {
		selected = skeleton.bones[boneList.value];
		runtimeBoneName.text = selected.boneID;
	}

	private void refreshOptions () {
		int i = boneList.value;
		boneList.ClearOptions ();
		List<Dropdown.OptionData> content = new List<Dropdown.OptionData> ();

		foreach (Models.Bone bone in skeleton.bones) {
			Dropdown.OptionData boneOption = new Dropdown.OptionData (bone.boneID);
			content.Add (boneOption);
		}

		if (content.Count < 1) {
			content.Add (new Dropdown.OptionData ("No Bones Yet!"));
		}
		boneList.AddOptions (content);
		boneList.value = i;
	}
	#endregion bone selection

	#region event hell

	public Toggle orbitMode, flyMode;
	enum CameraAnchorModeEnum { orbit, fly };
	CameraAnchorModeEnum currentCameraAnchor = CameraAnchorModeEnum.orbit;
	public void cameraAnchorMode () {
		if (orbitMode.isOn) {
			currentCameraAnchor = CameraAnchorModeEnum.orbit;
		} else {
			currentCameraAnchor = CameraAnchorModeEnum.fly;
		}
	}


	public InputField keyframeID;
	public Models.Pose selectedKF = null;

	public void RemoveKeyframe () {
		if (selectedKF != skeleton.poses[0])   //do not allow deleting t-pose
			_removePose (selectedKF);
	}

	public void KeyframeSelected () {
		int i = keyframeSelector.value;
		if (i > skeleton.poses.Length - 1) {

		}
		selectedKF = skeleton.poses[i];
		keyframeID.text = selectedKF.name;
	}

	public void KeyframeRenamed () {
		//todo: do not allow duplicates
		if (selectedKF == null) {
			selectedKF = skeleton.poses[0];
			return;
		}
		if (selectedKF == skeleton.poses[0]) {
			keyframeID.text = selectedKF.name;
		}
		selectedKF.name = keyframeID.text;
	}

	public void AddKeyframe () {
		Models.Pose newKF = new Models.Pose ();
		newKF.name = "Unnamed";



		_addPose (newKF);
		//todo: copy t-pose to new frame by default.
		selectedKF = newKF;
		_updateKeyFrameUI ();
	}

	public GameObject positioningMenu, camAnchorMenu;
	enum FocusModeEnum { camera, bone };
	FocusModeEnum currentFocusMode = FocusModeEnum.camera;
	public Toggle cameraMode, boneMode;
	public void focusModeChanged () {
		if (cameraMode.isOn) {
			currentFocusMode = FocusModeEnum.camera;
		} else {
			currentFocusMode = FocusModeEnum.bone;
		}
		onChangeInToggles ();
	}


	public Toggle translateMode, anchorMode, rotateMode;
	enum MovementModeEnum { translate, anchor, rotate };
	MovementModeEnum currentMovementMode = MovementModeEnum.rotate;
	public void movementModeChanged () {
		if (translateMode.isOn) {
			currentMovementMode = MovementModeEnum.translate;
		} else if (anchorMode.isOn) {
			currentMovementMode = MovementModeEnum.anchor;
		} else if (rotateMode.isOn) {
			currentMovementMode = MovementModeEnum.rotate;
		}
		onChangeInToggles ();
	}

	#endregion toggle hell
	
	#region toggle logic
	public InputField xValue, yValue, zValue;
	public Toggle xSelect, ySelect, zSelect;
	public GameObject removeBoneButton;

	void updateInputs (Vector3 value) {
		xValue.text = "" + value.x;
		yValue.text = "" + value.y;
		zValue.text = "" + value.z;
	}


	float validateOrDefault (string updated, float value) {
		float nValue = 0;
		bool conversionIsSuccessful = float.TryParse (updated, out nValue);
		if (conversionIsSuccessful) {
			return nValue;
		} else {
			return value;
		}
	}

	Vector3 validateAndGetTextboxInput (Vector3 old) {
		Vector3 result = Vector3.zero;
		result.x = validateOrDefault (xValue.text, old.x);
		result.y = validateOrDefault (yValue.text, old.x);
		result.z = validateOrDefault (zValue.text, old.x);
		updateInputs (result);
		return result;
	}

	public void onChangeInToggles () {
		if (skeleton.bones.Length == 0) {
			cameraMode.isOn = true;
			boneMode.gameObject.SetActive (false);
		} else {
			boneMode.gameObject.SetActive (true);
		}
		if (currentFocusMode == FocusModeEnum.camera &
				currentMovementMode == MovementModeEnum.anchor) {
			positioningMenu.SetActive (false);
			camAnchorMenu.SetActive (true);
		} else {
			positioningMenu.SetActive (true);
			camAnchorMenu.SetActive (false);
		}
		switch (currentFocusMode) {
			case FocusModeEnum.bone:
				if (selected != null) {

					runtimeBoneName.gameObject.SetActive (true);
					runtimeBoneName.text = selected.boneID;
				} else {
					return;
				}
				switch (currentMovementMode) {
					case MovementModeEnum.anchor:
						updateInputs (selected.anchorRelative);

						//selected.anchorRelative.x = 
						break;
					case MovementModeEnum.rotate:
						break;
					case MovementModeEnum.translate:

						break;
				}
				_updateRenderPositioning ();
				break;
			case FocusModeEnum.camera:
				xValue.gameObject.SetActive (true);
				yValue.gameObject.SetActive (true);
				zValue.gameObject.SetActive (true);
				xSelect.gameObject.SetActive (true);
				ySelect.gameObject.SetActive (true);
				zSelect.gameObject.SetActive (true);
				runtimeBoneName.gameObject.SetActive (false);
				removeBoneButton.SetActive (false);
				//BoneSelected
				switch (currentMovementMode) {
					case MovementModeEnum.anchor:
						break;
					case MovementModeEnum.rotate:
						updateInputs (cameraGO.transform.rotation.eulerAngles);
						break;
					case MovementModeEnum.translate:
						updateInputs (cameraGO.transform.position);
						break;
				}
				break;
		}
	}
	#endregion

	#region UIEvents

	public void TextboxEditCompleted () {

		switch (currentFocusMode) {
			case FocusModeEnum.bone:
				if (selected != null) {
				} else {
					return;
				}
				switch (currentMovementMode) {
					case MovementModeEnum.anchor:
						selected.anchorRelative = validateAndGetTextboxInput (selected.anchorRelative);
						break;
					case MovementModeEnum.rotate:
						break;
					case MovementModeEnum.translate:

						break;
				}
				break;
			case FocusModeEnum.camera:
				switch (currentMovementMode) {
					case MovementModeEnum.anchor:
						break;
					case MovementModeEnum.rotate:
						cameraGO.transform.rotation = Quaternion.Euler (validateAndGetTextboxInput (cameraGO.transform.rotation.eulerAngles));
						break;
					case MovementModeEnum.translate:
						cameraGO.transform.position = validateAndGetTextboxInput (cameraGO.transform.position);
						break;
				}
				break;
		}


	}

	#endregion

	#region UIOut

	public Dropdown keyframeSelector;
	public InputField keyframeNameEdit;
	public Button removeKeyframeButton;
	private void _updateKeyFrameUI () {
		keyframeID.text = selectedKF.name;
		int prevSelected = -1;
		List<Dropdown.OptionData> options = new List<Dropdown.OptionData> ();
		for (int i = 0; i < skeleton.poses.Length; i++) {
			options.Add (new Dropdown.OptionData (skeleton.poses[i].name));
			if (skeleton.poses[i] == selectedKF) {
				prevSelected = i;
			}
		}
		keyframeSelector.ClearOptions ();
		keyframeSelector.AddOptions (options);
		if (prevSelected != -1) {
			keyframeSelector.value = prevSelected;
		} else {
			keyframeSelector.value = 0;
		}
		if (selectedKF.name.Equals (tPose.name)) {
			removeKeyframeButton.gameObject.SetActive (false);
			keyframeNameEdit.gameObject.SetActive (false);
		} else if (selectedKF != null) {
			removeKeyframeButton.gameObject.SetActive (true);
			keyframeNameEdit.gameObject.SetActive (true);
		}
	}

	public void _updateRenderGeometry () {
		DestroyImmediate (renderObject);
		renderObject = new GameObject ("render");
		renderObject.transform.parent = renderParent.transform;
		foreach (Models.Bone b in skeleton.bones) {
			b.inScene = b.image.render (renderObject, renderMaterial);
		}
		_validateSkeletonHierarchy ();
		_updateRenderPositioning ();
	}

	private Models.BoneState _byName (string id) {
		if(selectedKF == null) {
			Debug.Log ("haa?");
		}
		if(selectedKF.bones == null) {
			_revalidateArrays();
		}
		foreach(Models.BoneState b in selectedKF.bones) {
			if (b.name.Equals (id)) {
				return b;
			}
		}
		return null;
	}

	public void _updateRenderPositioning () {
		foreach (Models.Bone b in skeleton.bones) {
			GameObject go = b.inScene;
			Models.BoneState state = _byName (b.boneID);
			if (state == null) {
				Debug.Log ("Couldn't find bone ID: " + b.boneID);
				continue;
			}
			if (b.inScene != null) {
				b.inScene.transform.localPosition = state.position;
				b.inScene.transform.localRotation = state.orientation;
			}
			
		}
	}

	bool hasBoneLoop = false;

	public void _validateSkeletonHierarchy () {
		//first, check for loops, which make an invalid skeleton
		foreach (Models.Bone b in skeleton.bones) {
			List<string> previous = new List<string> ();
			Models.Bone next = skeleton.byName (b.parentBone);
			previous.Add (b.boneID);
			bool resolved = false;
			while (!resolved) {
				if (next != null) {
					if (previous.Contains (next.boneID)) {
						Debug.Log ("ERROR! LOOP IN SKELETON!");
						hasBoneLoop = true;
					}
					previous.Add (next.boneID);
					next = skeleton.byName (next.boneID);
				} else {
					resolved = true;
				}
			}
		}
		hasBoneLoop = false;
		//next, update skeleton hierarchy

		foreach (Models.Bone b in skeleton.bones) {
			b.parent = skeleton.byName (b.parentBone);
			if (b.parent != null) {
				b.inScene.transform.parent = b.parent.inScene.transform;
			} else {
				b.inScene.transform.parent = renderObject.transform;
			}
		}
	}

	#endregion

	#region helper functions


	public void _copyTPose (Models.Pose target) {
		int i = 0;
		foreach (Models.BoneState bone in tPose.bones) {
			target.bones[i].name = bone.name;
			target.bones[i].orientation = bone.orientation;
			target.bones[i].position = bone.position;
		}
	}

	public void _removePose (Models.Pose frame) {
		List<Models.Pose> magicBox = new List<Models.Pose> (skeleton.poses);
		magicBox.Remove (frame);
		skeleton.poses = magicBox.ToArray ();
	}
	public void _addPose (Models.Pose frame) {
		List<Models.Pose> magicBox = new List<Models.Pose> (skeleton.poses);
		magicBox.Add (frame);
		skeleton.poses = magicBox.ToArray ();
	}

	public void _removeBone (Models.Bone bone) {
		List<Models.Bone> magicBox = new List<Models.Bone> (skeleton.bones);
		magicBox.Remove (bone);
		skeleton.bones = magicBox.ToArray ();
	}
	public void _addBone (Models.Bone bone) {
		List<Models.Bone> magicBox = new List<Models.Bone> (skeleton.bones);
		magicBox.Add (bone);
		skeleton.bones = magicBox.ToArray ();
		refreshOptions ();
	}
	public void _renameBone (string oldName, string newName) {

	}

	#endregion

	public void _revalidateArrays () {
		//this will make sure we have enough bone slots in all our classes
		if (skeleton == null) {
			skeleton = new Models.AnimationSkeleton ();
			skeleton.poses = new Models.Pose[0];
			tPose = new Models.Pose ();
			skeleton.bones = new Models.Bone[0];
			tPose.bones = new Models.BoneState[skeleton.bones.Length];
			tPose.name = "T-Pose";
			selectedKF = tPose;
			_addPose (tPose);
			_updateKeyFrameUI ();
			skeleton.bones = new Models.Bone[0];
		}
		for (int i = 0; i < skeleton.poses.Length; i++) {
			foreach (Models.Bone b in skeleton.bones) {
				if (skeleton.poses[i]._byName (b.boneID) == null) {
					Models.BoneState newState = new Models.BoneState ();
					newState.name = b.boneID;

					skeleton.poses[i]._addBoneState (newState);
				}
			}
		}
	}

	private void OnEnable () {
		_revalidateArrays ();
		_updateRenderGeometry ();
		_updateRenderPositioning();
		refreshOptions ();
		onChangeInToggles ();
		KeyframeSelected ();
		selected = null;
	}

	private void Start () {
		OnEnable ();
	}

}
