﻿Shader "Custom/VertexColor"
{
	Properties
	{
		_MainTex("Albedo", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)

		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

		_BumpScale("Scale", Float) = 1.0
		_BumpMap("Normal Map", 2D) = "bump" {}

		_EmissionColor("Color", Color) = (0,0,0)
		_EmissionMap("Emission", 2D) = "white" {}

		_DistortionStrength("Strength", Float) = 1.0
		_DistortionBlend("Blend", Range(0.0, 1.0)) = 0.5

		_SoftParticlesNearFadeDistance("Soft Particles Near Fade", Float) = 0.0
		_SoftParticlesFarFadeDistance("Soft Particles Far Fade", Float) = 1.0
		_CameraNearFadeDistance("Camera Near Fade", Float) = 1.0
		_CameraFarFadeDistance("Camera Far Fade", Float) = 2.0

			// Hidden properties

		[HideInInspector] _MetallicGlossMap("Metallic", 2D) = "white" {}
		[HideInInspector] [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 1.0
		[HideInInspector] _Glossiness("Smoothness", Range(0.0, 1.0)) = 1.0
			[HideInInspector] _Mode("__mode", Float) = 0.0
			[HideInInspector] _FlipbookMode("__flipbookmode", Float) = 0.0
			[HideInInspector] _LightingEnabled("__lightingenabled", Float) = 1.0
			[HideInInspector] _DistortionEnabled("__distortionenabled", Float) = 0.0
			[HideInInspector] _EmissionEnabled("__emissionenabled", Float) = 0.0
			[HideInInspector] _BlendOp("__blendop", Float) = 0.0
			[HideInInspector] _SrcBlend("__src", Float) = 1.0
			[HideInInspector] _DstBlend("__dst", Float) = 0.0
			[HideInInspector] _ZWrite("__zw", Float) = 1.0
			[HideInInspector] _Cull("__cull", Float) = 2.0
			[HideInInspector] _SoftParticlesEnabled("__softparticlesenabled", Float) = 0.0
			[HideInInspector] _CameraFadingEnabled("__camerafadingenabled", Float) = 0.0
			[HideInInspector] _SoftParticleFadeParams("__softparticlefadeparams", Vector) = (0,0,0,0)
			[HideInInspector] _CameraFadeParams("__camerafadeparams", Vector) = (0,0,0,0)
			[HideInInspector] _DistortionStrengthScaled("__distortionstrengthscaled", Float) = 0.0
	}

		SubShader
		{
			Tags { "RenderType" = "Opaque" "IgnoreProjector" = "True" "PreviewType" = "Plane" "PerformanceChecks" = "False" }

			BlendOp[_BlendOp]
			Blend[_SrcBlend][_DstBlend]
			ZWrite[_ZWrite]
			Cull[_Cull]

			GrabPass
			{
				Tags { "LightMode" = "Always" }
				"_GrabTexture"
			}

			Pass
			{
				Name "ShadowCaster"
				Tags { "LightMode" = "ShadowCaster" }

				BlendOp Add
				Blend One Zero
				ZWrite On
				Cull Off

				CGPROGRAM
				#pragma target 3.0

				#pragma shader_feature_local _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON _ALPHAMODULATE_ON
				#pragma shader_feature_local _METALLICGLOSSMAP
				#pragma shader_feature_local _REQUIRE_UV2
				#pragma multi_compile_shadowcaster
				#pragma multi_compile_instancing
				#pragma instancing_options procedural:vertInstancingSetup

				#pragma vertex vertParticleShadowCaster
				#pragma fragment fragParticleShadowCaster

				#include "UnityStandardParticleShadow.cginc"
				ENDCG
			}

			Pass
			{
				Name "SceneSelectionPass"
				Tags { "LightMode" = "SceneSelectionPass" }

				BlendOp Add
				Blend One Zero
				ZWrite On
				Cull Off

				CGPROGRAM
				#pragma target 3.0

				#pragma shader_feature_local _ALPHATEST_ON
				#pragma shader_feature_local _REQUIRE_UV2
				#pragma multi_compile_instancing
				#pragma instancing_options procedural:vertInstancingSetup

				#pragma vertex vertEditorPass
				#pragma fragment fragSceneHighlightPass

				#include "UnityStandardParticleEditor.cginc"
				ENDCG
			}

			Pass
			{
				Name "ScenePickingPass"
				Tags{ "LightMode" = "Picking" }

				BlendOp Add
				Blend One Zero
				ZWrite On
				Cull Off

				CGPROGRAM
				#pragma target 3.0

				#pragma shader_feature_local _ALPHATEST_ON
				#pragma shader_feature_local _REQUIRE_UV2
				#pragma multi_compile_instancing
				#pragma instancing_options procedural:vertInstancingSetup

				#pragma vertex vertEditorPass
				#pragma fragment fragScenePickingPass

				#include "UnityStandardParticleEditor.cginc"
				ENDCG
			}

			CGPROGRAM
			#pragma surface surf Standard nolightmap nometa noforwardadd keepalpha vertex:vert
			#pragma multi_compile __ SOFTPARTICLES_ON
			#pragma multi_compile __ SHADOWS_SHADOWMASK
			#pragma multi_compile_instancing
			#pragma instancing_options procedural:vertInstancingSetup
			#pragma target 3.0

			#pragma shader_feature_local _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON _ALPHAMODULATE_ON
			#pragma shader_feature_local _METALLICGLOSSMAP
			#pragma shader_feature_local _NORMALMAP
			#pragma shader_feature _EMISSION
			#pragma shader_feature_local _FADING_ON
			#pragma shader_feature_local _REQUIRE_UV2
			#pragma shader_feature_local EFFECT_BUMP

			#include "UnityStandardParticles.cginc"
			ENDCG
		}

			Fallback "VertexLit"
			CustomEditor "StandardParticlesShaderGUI"
}
/*Shader "Custom/VertexColor"
{

	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#pragma target 3.0
		struct Input
		{
			float4 vertColor;
		};

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = GammaToLinearSpace(IN.vertColor.rgb);
		}
		ENDCG
	}

		FallBack "Diffuse"

}*/

/*
Shader "Custom/VertexColor" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}
		SubShader{
			Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
			LOD 200

			CGPROGRAM
			#pragma surface surf Lambert alpha

			sampler2D _MainTex;

			struct Input {
				float2 uv_MainTex;
				float4 color: Color; // Vertex color
			};

			void surf(Input IN, inout SurfaceOutput o) {
				half4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb * IN.color.rgb; // vertex RGB
				o.Alpha = c.a * IN.color.a; // vertex Alpha
			}
			ENDCG
	}
		FallBack "Diffuse"
}*/
//technically works, but only for red??
/*Shader "Custom/VertexColor" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_SpecColor("Spec Color", Color) = (1,1,1,1)
		_Emission("Emmisive Color", Color) = (0,0,0,0)
		_Shininess("Shininess", Range(0.01, 1)) = 0.7
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		SubShader{
			Pass {
				Material {
					Shininess[_Shininess]
					Specular[_SpecColor]
					Emission[_Emission]
				}
				ColorMaterial AmbientAndDiffuse
				Lighting On
				SeparateSpecular On
				SetTexture[_MainTex] {
					Combine texture * primary, texture * primary
				}
				SetTexture[_MainTex] {
					constantColor[_Color]
					Combine previous * constant DOUBLE, previous * constant
				}
			}
	}

		Fallback " VertexLit", 1
}
*/
//technically works, but only for red??
/*Shader "Custom/VertexColor" {
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#pragma target 3.0

		struct Input {
			float4 vertColor : COLOR;
		};

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = IN.vertColor.rgb;
		}
		ENDCG
	}
}*/